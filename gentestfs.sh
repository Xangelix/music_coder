#!/usr/bin/env bash

prefix=testfs
mkdir $prefix
for x in {a..z}
do
    mkdir "$prefix/$x"
    for y in {a..z}
    do
        mkdir "$prefix/$x/$y"
            for z in {a..g}
            do
                touch "$prefix/$x/$y/$z"
            done
    done
done

