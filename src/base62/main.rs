use base_62::{decode, encode};
use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Positional commands
    proc: String,

    /// Show changes without executing them
    #[arg(short, long)]
    decode: bool,
}

fn main() {
    let cli = Cli::parse();
    if cli.decode {
        let dec = &decode(&cli.proc).expect("msg");
        let enc = core::str::from_utf8(dec).expect("msg");
        println!("{}", enc);
    } else {
        let enc = encode(cli.proc.as_bytes());
        println!("{}", enc);
    }
}
