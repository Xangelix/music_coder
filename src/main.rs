//! Init from cli
use clap::{Parser, Subcommand};
use std::path::PathBuf;
use walkdir::WalkDir;

use data_encoding::HEXLOWER;
use ring::digest::{Context as RingContext, Digest, SHA256};
use std::fs::File;
use std::io::{BufReader, Read};
use std::{fs, usize};

use anyhow::Result;
use comfy_table::modifiers::UTF8_ROUND_CORNERS;
use comfy_table::presets::UTF8_FULL;
use comfy_table::Table;

use base_62::{decode, encode};

/// Custom encoding for music files
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Positional commands
    #[command(subcommand)]
    command: Option<Commands>,
}

/// Positional arguments
#[derive(Subcommand)]
enum Commands {
    /// Encodes dir recursive
    Encode {
        /// Directory to encode
        dir: PathBuf,

        /// Increase verbosity [1-3]
        #[arg(short, long, action = clap::ArgAction::Count)]
        verbose: u8,

        /// Show changes without executing them
        #[arg(short, long)]
        noaction: bool,
    },
    /// Decodes dir recursive
    Decode {
        /// Directory to decode
        dir: PathBuf,

        /// Increase verbosity [1-3]
        #[arg(short, long, action = clap::ArgAction::Count)]
        verbose: u8,

        /// Show changes without executing them
        #[arg(short, long)]
        noaction: bool,
    },
}

/// Take sha256 of a file
fn sha256_digest<R: Read>(mut reader: R) -> Result<Digest> {
    let mut context = RingContext::new(&SHA256);
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    return Ok(context.finish());
}
use core::convert::TryFrom;
use indicatif::ProgressBar;

fn run_encode(dir: PathBuf, verbose: u8, noaction: bool) {
    let protected = vec![];
    let skip = vec!["log", "txt", "pdf", "png", "jpg", "jpeg", "tiff"];
    let mut table = Table::new();
    table
        .load_preset(UTF8_FULL)
        .apply_modifier(UTF8_ROUND_CORNERS)
        .set_header(vec!["Type", "Source", "Destination"]);

    let bar = ProgressBar::new(
        u64::try_from(
            WalkDir::new(dir.clone())
                .contents_first(true)
                .into_iter()
                .count(),
        )
        .expect("wow"),
    );
    for entry in WalkDir::new(dir).contents_first(true) {
        bar.inc(1);
        let entry = match entry {
            Ok(entry) => entry,
            Err(_) => panic!("Failed to read entry"),
        };

        let path = entry.path().to_str().expect("wow");

        let metadata = fs::metadata(path).expect("wow");
        let file_type = metadata.file_type();

        let mut objtype = "Dir";
        let ext: String = entry.path().extension().map_or_else(
            || return String::new(),
            |ext| return ext.to_str().expect("ok").to_owned(),
        );
        let parent;
        if file_type.is_file() && !skip.contains(&ext.as_str()) {
            let input = File::open(path).expect("msg");

            let reader = BufReader::new(input);
            let digest = sha256_digest(reader).expect("asda");
            objtype = "File";

            parent = entry
                .path()
                .parent()
                .unwrap()
                .to_str()
                .expect("ok")
                .to_owned();

            let digest = HEXLOWER.encode(digest.as_ref());
            let dst_file = if !ext.is_empty() {
                if protected.contains(&ext.as_str()) {
                    path.to_owned()
                } else {
                    format!("{parent}/{digest}.{ext}")
                }
            } else {
                digest
            };

            table.add_row(vec![objtype, path, &dst_file]);
            if !noaction {
                match fs::rename(path, dst_file.clone()) {
                    Ok(_) => (),
                    Err(_) => panic!("Could not rename {path} to {dst_file}."),
                }
            }
        } else {
            let fln = if skip.contains(&ext.as_str()) {
                entry
                    .path()
                    .file_stem()
                    .expect("msg")
                    .to_str()
                    .expect("msg")
            } else {
                entry
                    .path()
                    .file_name()
                    .expect("msg")
                    .to_str()
                    .expect("msg")
            };
            let enc = if skip.contains(&ext.as_str()) {
                format!("{}.{}", encode(fln.as_bytes()), ext.as_str())
            } else {
                encode(fln.as_bytes())
            };
            parent = entry
                .path()
                .parent()
                .unwrap()
                .to_str()
                .expect("ok")
                .to_owned();

            table.add_row(vec![objtype, path, &format!("{parent}/{enc}")]);
            if !noaction {
                match fs::rename(path, format!("{parent}/{enc}")) {
                    Ok(_) => (),
                    Err(_) => panic!(
                        "Could not rename {} to {}.",
                        path,
                        format!("{parent}/{enc}")
                    ),
                }
            }
        }
    }

    if verbose > 0 {
        println!("{table}");
    }
}

/// Decode
fn run_decode(dir: PathBuf, verbose: u8, noaction: bool) {
    let protected = vec![];
    let skip = vec!["log", "txt", "pdf", "png", "jpg", "jpeg", "tiff"];
    let subject = vec!["flac", "mp3"];
    let mut table = Table::new();
    table
        .load_preset(UTF8_FULL)
        .apply_modifier(UTF8_ROUND_CORNERS)
        .set_header(vec!["Type", "Source", "Destination"]);
    if verbose > 1 {
        println!(
            "Running decode on {} with noaction {}...",
            dir.as_path().display(),
            noaction
        );
    }

    let bar = ProgressBar::new(
        u64::try_from(
            WalkDir::new(dir.clone())
                .contents_first(true)
                .into_iter()
                .count(),
        )
        .expect("wow"),
    );
    for entry in WalkDir::new(dir).contents_first(true) {
        bar.inc(1);
        let entry = match entry {
            Ok(entry) => entry,
            Err(_) => panic!("Failed to read entry"),
        };

        let path = entry.path().to_str().expect("wow");

        let fs_metadata = fs::metadata(path).expect("wow");
        let file_type = fs_metadata.file_type();
        let mut objtype = "Dir";

        let parent;
        let ext = match entry.path().extension() {
            Some(ex) => ex.to_str().expect("ok").to_owned(),
            None => String::new(),
        };

        if file_type.is_file() && !skip.contains(&ext.as_str()) {
            objtype = "File";
            parent = entry
                .path()
                .parent()
                .unwrap()
                .to_str()
                .expect("ok")
                .to_owned();

            let dst_file = match entry.path().extension() {
                Some(ext) => {
                    let extstr = ext.to_str().expect("ok");
                    if protected.contains(&extstr) {
                        path.to_owned()
                    } else {
                        let mut name = "UNKNOWN".to_owned();
                        if subject.contains(&extstr) {
                            let mut meta = metadata::MediaFileMetadata::new(&path).expect("ok");
                            meta.include_all_tags(true);
                            let audio = meta.tags;

                            let mut album = String::new();
                            let mut album_artist = String::new();
                            let mut total_discs = String::new();
                            let mut total_tracks = String::new();

                            let mut artist = String::new();
                            let mut artists = String::new();
                            let mut disc = String::new();
                            let mut track = String::new();
                            let mut title = String::new();

                            for tag in &audio {
                                if tag.0.eq(&"ALBUM") {
                                    album = tag.1.clone();
                                } else if tag.0.eq(&"album_artist") {
                                    album_artist = tag.1.clone();
                                } else if tag.0.eq(&"TOTALDISCS") {
                                    total_discs = tag.1.clone();
                                } else if tag.0.eq(&"TOTALTRACKS") {
                                    total_tracks = tag.1.clone();
                                } else if tag.0.eq(&"ARTIST") {
                                    artist = tag.1.clone();
                                } else if extstr.eq("flac") && tag.0.eq(&"ARTISTS") {
                                    artists = tag.1.clone();
                                } else if extstr.eq("mp3") && tag.0.eq(&"ARTISTS") {
                                    artists = tag.1.clone().replace('/', ";");
                                } else if extstr.eq("flac") && tag.0.eq(&"disc") {
                                    disc = tag.1.clone();
                                } else if extstr.eq("mp3") && tag.0.eq(&"disc") {
                                    let sp: Vec<&str> = tag.1.split('/').collect();
                                    disc = sp.first().expect("wow").to_owned().to_owned();
                                    total_discs = sp.get(1).expect("wow").to_owned().to_owned();
                                } else if extstr.eq("flac") && tag.0.eq(&"track") {
                                    track = tag.1.clone();
                                } else if extstr.eq("mp3") && tag.0.eq(&"track") {
                                    let sp: Vec<&str> = tag.1.split('/').collect();
                                    track = sp.first().expect("wow").to_owned().to_owned();
                                    total_tracks = sp.get(1).expect("wow").to_owned().to_owned();
                                } else if (extstr.eq("flac") && tag.0.eq(&"TITLE"))
                                    || (extstr.eq("mp3") && tag.0.eq(&"title"))
                                {
                                    title = tag.1.clone();
                                }
                            }

                            let mut first_matched = false;
                            let mut total_match = false;
                            let ans;
                            let mut first_idx = 0;
                            if artist.contains(';') {
                                let artist_split: Vec<&str> = artist.split(';').collect();
                                let mut artists_split: Vec<&str> = artists.split(';').collect();

                                let mut first_split = String::new();
                                let mut second_split = String::new();

                                for (_ai, artist_i) in artist_split.iter().enumerate() {
                                    for (asi, artists_i) in artists_split.iter().enumerate() {
                                        if !first_matched && artist_i.ends_with(artists_i) {
                                            first_split = artists_i.to_owned().to_owned();
                                            first_idx = asi;
                                            first_matched = true;
                                        } else if first_matched && artist_i.starts_with(artists_i) {
                                            second_split = artists_i.to_owned().to_owned();
                                            total_match = true;
                                        }
                                    }
                                }

                                if total_match {
                                    artists_split.remove(first_idx);
                                    artists_split.remove(first_idx);
                                    ans = format!("{};{}", first_split.clone(), second_split.clone());
                                    artists_split.insert(
                                        first_idx,
                                        &ans,
                                    );
                                }
                                artists = artists_split.join("; ");
                            } else {
                                artists = artists.replace(';', "; ");
                            }

                            // Picard Reference
                            //
                            //$if2(%albumartist%,%artist%)/
                            //$if(%albumartist%,%album%/,)
                            //$if(
                            //    $gt(%totaldiscs%,1),
                            //    $if(
                            //        $gt(%totaldiscs%,9),
                            //        $num(%discnumber%,2),
                            //        %discnumber%
                            //     )-,
                            //   )
                            //$if(
                            //    $and(%albumartist%,%tracknumber%),
                            //    $if(
                            //        $gt(%totaltracks%, 99),
                            //        $num(%tracknumber%,3),
                            //        $num(%tracknumber%,2)
                            //    ) ,
                            //)- %artists% - %title%

                            // Build root off album_artist if exists, otherwise use artist
                            // /album_artist,artist/
                            let _final_artist = if !album_artist.is_empty() {
                                album_artist.clone()
                            } else {
                                artist.clone()
                            };

                            let _artist_gap = if !album_artist.is_empty() {
                                format!("{album}/")
                            } else {
                                String::new()
                            };

                            let total_discs_num: usize = match total_discs.parse() {
                                Ok(num) => num,
                                Err(_) => panic!("{path}"),
                            };
                            let disc_num: usize = if total_discs_num > 1 {
                                disc.parse().expect(&format!("ok {path}"))
                            } else {
                                0
                            };
                            let disc_gap = if total_discs_num > 1 {
                                if total_discs_num > 9 {
                                    format!("{disc_num:02}-")
                                } else {
                                    format!("{disc}-")
                                }
                            } else {
                                String::new()
                            };

                            let total_tracks_num: usize = match total_tracks.parse() {
                                Ok(num) => num,
                                Err(_) => panic!("{path}"),
                            };
                            let track_num: usize = track.parse().expect("ok");
                            let final_track = if !album_artist.is_empty() && !track.is_empty() {
                                if total_tracks_num > 99 {
                                    format!("{track_num:03} ")
                                } else {
                                    format!("{track_num:02} ")
                                }
                            } else {
                                String::new()
                            };

                            //let out = format!("{final_artist}/{disc_gap}{final_track}- {artists} - {title}");
                            name = format!("{disc_gap}{final_track}- {artists} - {title}");
                        }
                        assert!(!name.eq("UNKNOWN"), "{path}");

                        name = name.replace('/', "_");

                        format!("{parent}/{name}.{extstr}")
                    }
                }
                None => path.to_owned(),
            };

            table.add_row(vec![objtype, path, &dst_file]);
            if !noaction {
                match fs::rename(path, dst_file) {
                    Ok(_) => (),
                    Err(_) => panic!("{path}"),
                }
            }
        } else {
            let fln = if skip.contains(&ext.as_str()) {
                entry
                    .path()
                    .file_stem()
                    .expect("msg")
                    .to_str()
                    .expect("msg")
            } else {
                entry
                    .path()
                    .file_name()
                    .expect("msg")
                    .to_str()
                    .expect("msg")
            };
            let dec = &decode(fln).expect("msg");
            let enc = if skip.contains(&ext.as_str()) {
                format!(
                    "{}.{}",
                    core::str::from_utf8(dec).expect("msg"),
                    ext.as_str()
                )
            } else {
                core::str::from_utf8(dec).expect("msg").to_owned()
            };

            parent = entry
                .path()
                .parent()
                .unwrap()
                .to_str()
                .expect("ok")
                .to_owned();
            table.add_row(vec![objtype, path, &format!("{parent}/{enc}")]);
            if !noaction {
                match fs::rename(path, format!("{parent}/{enc}")) {
                    Ok(_) => (),
                    Err(_) => panic!("{path}"),
                }
            }
        }
    }

    if verbose > 0 {
        println!("{table}");
    }
}

fn main() {
    let cli = Cli::parse();

    match cli.command {
        Some(Commands::Encode {
            dir,
            verbose,
            noaction,
        }) => {
            run_encode(dir, verbose, noaction);
        }
        Some(Commands::Decode {
            dir,
            verbose,
            noaction,
        }) => {
            run_decode(dir, verbose, noaction);
        }
        None => {}
    }
}
