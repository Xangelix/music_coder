#!/usr/bin/env bash

# Don't modify the original, encode a 'staging' copy
cp -r $1 $1-staging
cargo run --release --bin music_coder -- encode $1-staging
ret=$?
if [ $ret -ne 0 ]; then
        echo "First failed."
        exit
fi

# Create then decode a double of the encoded dir
cp -r $(dirname $1)/$(cargo run  --release --bin base62 -- "$(basename $1)-staging") $(dirname $1)/$(cargo run  --release --bin base62 -- "$(basename $1)-double")
cargo run --release --bin music_coder -- decode $(dirname $1)/$(cargo run  --release --bin base62 -- "$(basename $1)-double")
ret=$?
if [ $ret -ne 0 ]; then
        echo "Second failed."
        exit
fi

# Find diffs between original and decoded
echo "Printing any diffs..."
diff -rq $1 $1-double
ret=$?
if [ $ret -ne 0 ]; then
        echo "Third failed."
        exit
fi
echo "If nothing was printed above, $1-double is safe to delete."
